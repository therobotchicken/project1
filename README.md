The program superimposes a stereogram, created using a depth map and a 128x128 tile, onto a face captured by a camera.
Due to some people having difficulty viewing stereograms, code has been included to show the depth map being used to the right. Trust me, it's in the stereogram.
The program is also intended to hide the background, but inability to stop the program from learning and lock the camera's exposure setting gave a less than desirable result.
![example run](/exampleRun.PNG)