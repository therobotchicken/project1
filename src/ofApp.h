#pragma once

#include "ofMain.h"
#include "ofxAutostereogram.h"
#include "ofxFaceTracker2.h"
#include "ofxVibe.h"

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
		
		ofxFaceTracker2 tracker;
		ofVideoGrabber webcam;
		ofxVibe vibe;
		ofImage frame;
		ofImage tile, depth, result;
		glm::vec2 nose;
		ofImage dm1;
		ofImage dm2;
		ofImage dm3;
		ofImage dm4;
		vector<ofImage> depthMaps;
		int currDM;
};
