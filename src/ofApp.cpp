#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
	//setup grabber and tracker, and provide location of face structure information
	ofSetDataPathRoot("data/model/");
	//webcam.setup(1280, 960);	//use for size, but runs very slowly
	webcam.setup(640,480);		//use for performance but may be hard to make out stereogram
	tracker.setup();
	vibe.setup(3, 30, 4, 17, 2, 16);
	frame.allocate(webcam.getWidth(), webcam.getHeight(), OF_IMAGE_COLOR);

	//tile
	tile.load("../mms.jpg");
	tile.resize(128, 128);
	//depth maps
	dm1.load("../eagleworldanchor.jpg");
	depthMaps.push_back(dm1);
	dm2.load("../face.png");
	depthMaps.push_back(dm2);
	dm3.load("../skull.png");
	depthMaps.push_back(dm3);
	dm4.load("../skullAndBones.png");
	depthMaps.push_back(dm4);
	currDM = 0;
	depth = depthMaps.at(0);
	depth.setImageType(OF_IMAGE_GRAYSCALE);
	//stereogram
	result.allocate(depth.getWidth() + tile.getWidth(), depth.getHeight(), OF_IMAGE_COLOR);
	ofxAutostereogram::makeAutostereogram(tile, depth, .2, result);
	result.update();
	result.setAnchorPercent(0.5, 0.66);
}

//--------------------------------------------------------------
void ofApp::update(){
	//update and resize webcam image and then update tracker
	if (webcam.isInitialized())
	{
		webcam.update();
		if (webcam.isFrameNew())
		{
			cv::Mat cvFrame = ofxCv::toCv(webcam);
			vibe.update(cvFrame);
			frame.setFromPixels(webcam.getPixels());

			//ideally wanted to use the next two lines to upscale vibe's output, but couldn't set pixels without crashing on startup
			//frame.setFromPixels(vibe.getOfForeground());
			//frame.resize(1280, 960);

			tracker.update(frame);
		}
	}
}

//--------------------------------------------------------------
void ofApp::draw(){
	//frame.draw(0, 0);		//see line 47

	//uncomment this to see what you should be seeing.
	//vibe.drawForeground(0, 0);
	//depthMaps.at(currDM).draw(641, 0, 640, 480);

	tracker.drawDebug();
	tracker.drawDebugPose();

	//draw the stereogram over each face; scales based on size of face, measured as distance from chin to eyebrow.
	for (auto face : tracker.getInstances())
	{
		float scale = (face.getLandmarks().getImagePoint(8).y - face.getLandmarks().getImagePoint(19).y) / result.getHeight() * 1.6;
		result.draw(face.getLandmarks().getImagePoint(33), result.getWidth() * scale, result.getHeight() * scale);
	}
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
	//change depth map by pressing left and right; loops back around if needed
	switch (key)
	{
	case OF_KEY_LEFT:
		if (currDM == 0)
			currDM = depthMaps.size() - 1;
		else
			currDM--;
		break;
	case OF_KEY_RIGHT:
		if (currDM == depthMaps.size() - 1)
			currDM = 0;
		else
			currDM++;
		break;
	}
	depth = depthMaps.at(currDM);
	depth.setImageType(OF_IMAGE_GRAYSCALE);
	result.allocate(depth.getWidth() + tile.getWidth(), depth.getHeight(), OF_IMAGE_COLOR);
	ofxAutostereogram::makeAutostereogram(tile, depth, .2, result);
	result.update();
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
